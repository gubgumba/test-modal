// SUBJECT :: Ajax Modal Slider

// Your API Request: http://www.aparat.com/etc/main/tagblog/type/fajr95/perpage/9
// Items: tagblog array
// Props: descr & date_str
// item_type : mihanblog(text), lenzor(image)
// Cross origin extntion needed

jQuery(document).ready(function() {
  const $ctaButton = jQuery("#callToActionButton");
  $ctaButton.on("click", event => {
    event.preventDefault();
    getSlider().then(res => {
      console.log("init...");
      const targetId = jQuery(event.target).attr("href");
      const targetElement = jQuery(targetId);
      magicModal.open({ data: res, width: "80vw", height: "80vh" });
    });
  });
});

function getSlider() {
  return jQuery.ajax({
    url: "data.json",
    method: "GET"
  });
}
